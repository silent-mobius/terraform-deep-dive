
---
# Terraform Setup

To use Terraform you will need to install it. HashiCorp distributes Terraform as a binary package, but also install Terraform using popular package managers.

---
# Terraform Setup (cont.)

To install Terraform on Debian based system use next list of commands:

```
sudo apt-get update && sudo apt-get install -y gnupg software-properties-common curl
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install terraform
```
---
# Terraform Setup (cont.)

To install Terraform on RedHat based system use next list of commands:

```
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo
sudo yum -y install terraform
```

> `[!]` Note: if you are using `Fedora` Linux you might need to use `dnf` instead `yum` and repository link might be different.

---
# Terraform Setup (cont.)

You can also, install Terraform manually, but install the Terraform manually. Download the terraform from the [link](terraform_1.0.4_linux_amd64.zip), unzip it, place somewhere on your `$PATH`, verify that it has run permissions to run and you are done.

```
wget https://releases.hashicorp.com/terraform/1.0.4/terraform_1.0.4_linux_amd64.zip -P /tmp
unzip /tmp/terraform_1.0.4_linux_amd64.zip
sudo mv /tmp/terraform /usr/local/bin/
sudo chmod 775 /usr/local/bin/terraform
```
> `[!]` Note: in case of manual install you might have to repeat the procedure, everytime version of terraform changes.

---
# Terraform Setup (cont.)

Working with shell can be tiresome, thus best thing you do is to get some help, with bash completion of terraform.
In our case, we just need to verify that out local `.bashrc` exists. and running `terraform -install-autocomplete`

