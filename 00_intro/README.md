# Terraform Deep Dive

.footer: Created by Alex M. Schapelle, VaioLabs.IO

---
# About The Course 

we'll learn several topics mainly focused on:

- what is IaC ?
- what is terraform ?
- who needs terraform ?
- how terraform works ?
- how to manage terraform in various scenatios ?

---

# Who Is This course for ?

- junior/senior sysadmins who have no knowledge of IaC automation with terraform
- for junior/senior developers who are interested in automating IaC with any stacks possible
- experienced ops who need refresher

---
# About Me

<img src="../misc/.img/me.jpg" alt="drawing" style="float:right;width:180px;">

- over 12 years of IT industry Experience.
- fell in love with IBM's AS-400 unix system at IDF.
- have studied in various schools for computer science.
    - between each semester, I tried to take IT course at various places.
        - I have earned several certifications, CCNA, LPIC1/2.
        - Other things I have learned alone.
---

# About Me (cont.)

- over 7 years of sysadmin:
    - [shell scripting](https://en.wikipedia.org/wiki/Shell_script) fanatic
    - [python](https://en.wikipedia.org/wiki/Python_(programming_language)) developer
    - [js](https://en.wikipedia.org/wiki/JavaScript) admirer
    - [golang](https://en.wikipedia.org/wiki/Go_(programming_language)) fallen
    - [rust](https://en.wikipedia.org/wiki/Rust_(programming_language)) fan
---  
# About Me (cont.)
- 5 years of working with devops
    - [git](https://en.wikipedia.org/wiki/Git) supporter
    - [vagrant](https://en.wikipedia.org/wiki/Vagrant_%28software%29) enthusiast
    - [ansible](https://en.wikipedia.org/wiki/Ansible_(software)) consultant
    - [container](https://en.wikipedia.org/wiki/Docker_(software)) believer
    - [k8s](https://en.wikipedia.org/wiki/Kubernetes) user
    - [terraform](https://en.wikipedia.org/wiki/Terraform_(software)) consultant

you can find me on linked-in: [Alex M. Schapelle](https://www.linkedin.com/in/alex-schapelle)

---
# Overview of IaC

- IaC grew as a response to the difficulty posed by utility computing and second-generation web frameworks. In 2006, the launch of AWS' Elastic Compute Cloud and the 1.0 version of Ruby on Rails just months before created widespread scaling problems in the enterprise that were previously experienced only at large, multi-national companies. 
  
---
# Overview of IaC (cont.)
- With new tools emerging to handle this ever growing field, the idea of IaC was born. The thought of modelling infrastructure with code, and then having the ability to design, implement, and deploy application infrastructure with known software best practices appealed to both software developers and IT infrastructure administrators. 
- The ability to treat infrastructure like code and use the same tools as any other software project would allow developers to rapidly deploy applications.

---
# Overview of Terraform

Terraform is an open-source infrastructure as code software tool created by HashiCorp. Users define and provide data center infrastructure using a declarative configuration language known as HashiCorp Configuration Language (HCL),