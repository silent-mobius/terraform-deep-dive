
---

# Terraform Language

---

# Terraform Language

The main purpose of the Terraform language is declaring resources, which represent infrastructure objects. All other language features exist only to make the definition of resources more flexible and convenient.

A Terraform configuration is a complete document in the Terraform language that tells Terraform how to manage a given collection of infrastructure. A configuration can consist of multiple files and directories.

---

# Terraform Language (cont.)

The syntax of the Terraform language consists of only a few basic elements:

```json
resource "aws_vpc" "main" {
  cidr_block = var.base_cidr_block
}

<BLOCK TYPE> "<BLOCK LABEL>" "<BLOCK LABEL>" {
  # Block body
  <IDENTIFIER> = <EXPRESSION> # Argument
}
```

---

- *Blocks* are containers for other content and usually represent the configuration of some kind of object, like a resource. Blocks have a block type, can have zero or more labels, and have a body that contains any number of arguments and nested blocks. Most of Terraform's features are controlled by top-level blocks in a configuration file.
- *Arguments* assign a value to a name. They appear within blocks.
- *Expressions* represent a value, either literally or by referencing and combining other values. They appear as values for arguments, or within other expressions.

---

The Terraform language is declarative, describing an intended goal rather than the steps to reach that goal. The ordering of blocks and the files they are organized into are generally not significant; Terraform only considers implicit and explicit relationships between resources when determining an order of operations

---

## File Extension

Code in the Terraform language is stored in plain text files with the .tf file extension. There is also a JSON-based variant of the language that is named with the .tf.json file extension.

Files containing Terraform code are often called configuration files.

---

### Text Encoding

Configuration files must always use UTF-8 encoding, and by convention usually use Unix-style line endings (LF) rather than Windows-style line endings (CRLF), though both are accepted.

---

### Directories and Modules

A module is a collection of `.tf` and/or `.tf.json` files kept together in a directory.

A Terraform module only consists of the top-level configuration files in a directory; nested directories are treated as completely separate modules, and are not automatically included in the configuration.

---

Terraform evaluates all of the configuration files in a module, effectively treating the entire module as a single document. Separating various blocks into different files is purely for the convenience of readers and maintainers, and has no effect on the module's behavior.

A Terraform module can use module calls to explicitly include other modules into the configuration. These child modules can come from local directories (nested in the parent module's directory, or anywhere else on disk), or from external sources like the Terraform Registry.

--- 

### The Root Module

Terraform always runs in the context of a single root module. A complete Terraform configuration consists of a root module and the tree of child modules (which includes the modules called by the root module, any modules called by those modules, etc.).

---

In Terraform CLI, the root module is the working directory where Terraform is invoked. (You can use command line options to specify a root module outside the working directory, but in practice this is rare. )

In Terraform Cloud and Terraform Enterprise, the root module for a workspace defaults to the top level of the configuration directory (supplied via version control repository or direct upload), but the workspace settings can specify a subdirectory to use instead.

---

## Syntax

The majority of the Terraform language documentation focuses on the practical uses of the language and the specific constructs it uses. The pages in this section offer a more abstract view of the Terraform language.

- Configuration Syntax describes the native grammar of the Terraform language.
- JSON Configuration Syntax documents how to represent Terraform language constructs in the pure JSON variant of the Terraform language. Terraform's JSON syntax is unfriendly to humans, but can be very useful when generating infrastructure as code with other systems that don't have a readily available HCL library.
- Style Conventions documents some commonly accepted formatting guidelines for Terraform code. These conventions can be enforced automatically with terraform fmt.

---

## Configuration Syntax

Other pages in this section have described various configuration constructs that can appear in the Terraform language. This page describes the lower-level syntax of the language in more detail, revealing the building blocks that those constructs are built from.

This page describes the native syntax of the Terraform language, which is a rich language designed to be relatively easy for humans to read and write. The constructs in the Terraform language can also be expressed in JSON syntax, which is harder for humans to read and edit but easier to generate and parse programmatically.

This low-level syntax of the Terraform language is defined in terms of a syntax called HCL, which is also used by configuration languages in other applications, and in particular other HashiCorp products. It is not necessary to know all of the details of HCL syntax in order to use Terraform, and so this page summarizes the most important details. If you are interested, you can find a full definition of HCL syntax in the HCL native syntax specification.

### Arguments and Blocks

The Terraform language syntax is built around two key syntax constructs: arguments and blocks.

### Arguments

An argument assigns a value to a particular name:

image_id = "abc123"

The identifier before the equals sign is the argument name, and the expression after the equals sign is the argument's value.

The context where the argument appears determines what value types are valid (for example, each resource type has a schema that defines the types of its arguments), but many arguments accept arbitrary expressions, which allow the value to either be specified literally or generated from other values programmatically.

Note: Terraform's configuration language is based on a more general language called HCL, and HCL's documentation usually uses the word "attribute" instead of "argument." These words are similar enough to be interchangeable in this context, and experienced Terraform users might use either term in casual conversation. But because Terraform also interacts with several other things called "attributes" (in particular, Terraform resources have attributes like id that can be referenced from expressions but can't be assigned values in configuration), we've chosen to use "argument" in the Terraform documentation when referring to this syntax construct.
### Blocks

A block is a container for other content:
```json
resource "aws_instance" "example" {
  ami = "abc123"

  network_interface {
    # ...
  }
}
```

A block has a type (resource in this example). Each block type defines how many labels must follow the type keyword. The resource block type expects two labels, which are aws_instance and example in the example above. A particular block type may have any number of required labels, or it may require none as with the nested network_interface block type.

After the block type keyword and any labels, the block body is delimited by the { and } characters. Within the block body, further arguments and blocks may be nested, creating a hierarchy of blocks and their associated arguments.

The Terraform language uses a limited number of top-level block types, which are blocks that can appear outside of any other block in a configuration file. Most of Terraform's features (including resources, input variables, output values, data sources, etc.) are implemented as top-level blocks.

## Identifiers

Argument names, block type names, and the names of most Terraform-specific constructs like resources, input variables, etc. are all identifiers.

Identifiers can contain letters, digits, underscores (_), and hyphens (-). The first character of an identifier must not be a digit, to avoid ambiguity with literal numbers.

For complete identifier rules, Terraform implements the Unicode identifier syntax, extended to include the ASCII hyphen character -.
»Comments

The Terraform language supports three different syntaxes for comments:

- \# begins a single-line comment, ending at the end of the line.
- \/\/ also begins a single-line comment, as an alternative to #.
- /* and */ are start and end delimiters for a comment that might span over multiple lines.

The # single-line comment style is the default comment style and should be used in most cases. Automatic configuration formatting tools may automatically transform // comments into # comments, since the double-slash style is not idiomatic.

---

## Character Encoding and Line Endings

Terraform configuration files must always be UTF-8 encoded. While the delimiters of the language are all ASCII characters, Terraform accepts non-ASCII characters in identifiers, comments, and string values.

Terraform accepts configuration files with either Unix-style line endings (LF only) or Windows-style line endings (CR then LF), but the idiomatic style is to use the Unix convention, and so automatic configuration formatting tools may automatically transform CRLF endings to LF.

---

## Resources

Resources are the most important element in the Terraform language. Each resource block describes one or more infrastructure objects, such as virtual networks, compute instances, or higher-level components such as DNS records.

- Resource Blocks documents the syntax for declaring resources.
- Resource Behavior explains in more detail how Terraform handles resource declarations when applying a configuration.
- The Meta-Arguments section documents special arguments that can be used with every resource type, including depends_on, count, for_each, provider, and lifecycle.
- Provisioners documents configuring post-creation actions for a resource using the provisioner and connection blocks. Since provisioners are non-declarative and potentially unpredictable, we strongly recommend that you treat them as a last resort.

---

## Resource Blocks


Resources are the most important element in the Terraform language. Each resource block describes one or more infrastructure objects, such as virtual networks, compute instances, or higher-level components such as DNS records.
»Resource Syntax

Resource declarations can include a number of advanced features, but only a small subset are required for initial use. More advanced syntax features, such as single resource declarations that produce multiple similar remote objects, are described later in this page.
```json
resource "aws_instance" "web" {
  ami           = "ami-a1b2c3d4"
  instance_type = "t2.micro"
}
```
A resource block declares a resource of a given type ("aws_instance") with a given local name ("web"). The name is used to refer to this resource from elsewhere in the same Terraform module, but has no significance outside that module's scope.

The resource type and name together serve as an identifier for a given resource and so must be unique within a module.

Within the block body (between { and }) are the configuration arguments for the resource itself. Most arguments in this section depend on the resource type, and indeed in this example both ami and instance_type are arguments defined specifically for the aws_instance resource type.

> `[!]` Note: Resource names must start with a letter or underscore, and may contain only letters, digits, underscores, and dashes.

## Resource Types

Each resource is associated with a single resource type, which determines the kind of infrastructure object it manages and what arguments and other attributes the resource supports.

## Providers

Each resource type is implemented by a provider, which is a plugin for Terraform that offers a collection of resource types. A provider usually provides resources to manage a single cloud or on-premises infrastructure platform. Providers are distributed separately from Terraform itself, but Terraform can automatically install most providers when initializing a working directory.

In order to manage resources, a Terraform module must specify which providers it requires. Additionally, most providers need some configuration in order to access their remote APIs, and the root module must provide that configuration.

For more information, see:

    Provider Requirements, for declaring which providers a module uses.
    Provider Configuration, for configuring provider settings.

Terraform usually automatically determines which provider to use based on a resource type's name. (By convention, resource type names start with their provider's preferred local name.) When using multiple configurations of a provider (or non-preferred local provider names), you must use the provider meta-argument to manually choose an alternate provider configuration. See the provider meta-argument for more details.

--- 

## Resource Arguments

Most of the arguments within the body of a resource block are specific to the selected resource type. The resource type's documentation lists which arguments are available and how their values should be formatted.

The values for resource arguments can make full use of expressions and other dynamic Terraform language features.

There are also some meta-arguments that are defined by Terraform itself and apply across all resource types. (See Meta-Arguments below.)

---

## Documentation for Resource Types

Every Terraform provider has its own documentation, describing its resource types and their arguments.

Most publicly available providers are distributed on the Terraform Registry, which also hosts their documentation. When viewing a provider's page on the Terraform Registry, you can click the "Documentation" link in the header to browse its documentation. Provider documentation on the registry is versioned, and you can use the dropdown version menu in the header to switch which version's documentation you are viewing.

To browse the publicly available providers and their documentation, see the providers section of the Terraform Registry.

Note: Provider documentation used to be hosted directly on terraform.io, as part of Terraform's core documentation. Although some provider documentation might still be hosted here, the Terraform Registry is now the main home for all public provider docs.

---

## Meta-Arguments

The Terraform language defines several meta-arguments, which can be used with any resource type to change the behavior of resources.

The following meta-arguments are documented on separate pages:

- depends_on, for specifying hidden dependencies
- count, for creating multiple resource instances according to a count
- for_each, to create multiple instances according to a map, or set of strings
- provider, for selecting a non-default provider configuration
- lifecycle, for lifecycle customizations
- provisioner, for taking extra actions after resource creation

---