
---
# Terraform Deep Dive

.footer: Created by Alex M. Schapelle, VaioLabs.IO

---
# Infrastructure As Code

- Infrastructure as code (IaC) is the process of managing and provisioning computer data centers through machine-readable definition files, rather than physical hardware configuration or interactive configuration tools. The IT infrastructure managed by this process comprises both physical equipment, such as bare-metal servers, as well as virtual machines, and associated configuration resources. the definitions may be in a version control system. It can use either scripts or declarative definitions, rather than manual processes, but the term is more often used to promote declarative approaches

---
# Infrastructure As Code (cont.)

- in short: No More Clicks.
- it can be saved as source code and managed in same manner.
- it enables automation on-prem and on cloud.
- IaC is declarative, in most of tools capabilities, which enables ease of of its automation.
- less human invervention during deployment means fewer chances of errors and security flaws.

---
# Declarative VS.  Imperative

There are generally two approaches to IaC: declarative (functional) vs. imperative (procedural). The difference between the declarative and the imperative approach is essentially **what** vs. **how** . The declarative approach focuses on what the eventual target configuration should be; the imperative focuses on how the infrastructure is to be changed to meet this. The declarative approach defines the desired state and the system executes what needs to happen to achieve that desired state. Imperative defines specific commands that need to be executed in the appropriate order to end with the desired conclusion.

---
# Methods

There are two methods of IaC: push and pull. The main difference is the manner in which the servers are told how to be configured.

- The pull method: the server to be configured will pull its configuration from the controlling server. 
- The push method: the controlling server pushes the configuration to the destination system.

---
# Tools


There are many tools that fulfill infrastructure automation capabilities and use IaC. Broadly speaking, any framework or tool that performs changes or configures infrastructure declaratively or imperatively based on a programmatic approach can be considered IaC. As these frameworks category expands, one of those categories, **Continuous configuration automation** (CCA) has become one of the most popular. CCA is the methodology or process of automating the deployment and configuration of settings and software for both physical and virtual data center equipment.

---
# Tools (cont.)

All CCA tools can be thought of as an extension of traditional IaC frameworks. They leverage IaC to change, configure, and automate infrastructure, and they also provide visibility, efficiency and flexibility in how infrastructure is managed. These additional attributes provide enterprise-level security and compliance. 

---
# Tools (cont.)
An important aspect when considering CCA tools, if they are **open source**, is the **community content**. As Gartner states, the value of CCA tools is “as dependent on user-community-contributed content and support as it is on the commercial maturity and performance of the automation tooling.” Vendors like Puppet and Chef, those that have been around a significant amount of time, have created their own communities.

---
# We already have tool for ...

- there are many tools already for IaC automation, e.g. ansible and AWSCLI, why would be need terraform ?
    - automate software defined networking
    - interact and takes case of communication with control layer api's with ease
    - supports a vast array of private and public vendors
    - track state of each resource deployed
