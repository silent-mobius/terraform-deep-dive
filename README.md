# Terraform Deep Dive

- [Intro](./00_intro/README.md)
- [Understanding IaC](./01_understanding_IaC/README.md)]
- [IaC with Terrafom](./02_IaC_with_terraform/README.md)]
- [Terraform fundamentals](./03_fundamentals/README.md)]
- [Terraform state file](./04_state/README.md)
- [Terraform modules](./05_modules/README.md)]
- [Terraform builtin functions and dynamic block](./06_built_in_and_dynamic_blocks/README.md)
- [Terraform CLI](./07_cli/README.md)]
- [Terraform cloud and enterpirze](./08_cloud_and_ent/README.md)